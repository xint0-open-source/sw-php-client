<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    ->withPhpSets(php81: true)
    ->withAttributesSets(phpunit: true)
    ->withTypeCoverageLevel(45)
    ->withDeadCodeLevel(44)
    ->withImportNames(removeUnusedImports: true);
