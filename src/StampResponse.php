<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb;

use JsonException;
use Psr\Http\Message\ResponseInterface;

class StampResponse
{
    /**
     * @var array<string,string>|null
     */
    public ?array $data = null;
    public string $message;
    public ?string $messageDetail = null;
    public string $status;
    public bool $success;

    public function __construct(private readonly ResponseInterface $response)
    {
        $this->parseResponseBody();
    }

    private function parseResponseBody(): void
    {
        $contents = $this->response->getBody()->getContents();
        $is_client_error = $this->response->getStatusCode() >= 400 && $this->response->getStatusCode() < 500;
        $is_server_error = $this->response->getStatusCode() >= 500;
        try {
            /**
             * @var array{
             *     data?:array<string,string>|null,
             *     message?:string,
             *     messageDetail?:string|null,
             *     status?:string
             * } $jsonResponse
             */
            $jsonResponse = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            $this->message = $is_server_error ? $contents : 'Could not decode JSON response.';
            $this->messageDetail = $is_server_error ? null : $exception->getMessage();
            $this->data = $is_server_error ? null : compact('contents');
            $this->status = 'error';
            $this->success = false;
            return;
        }

        $this->data = array_key_exists('data', $jsonResponse) ? $jsonResponse['data'] : null;
        $this->message = array_key_exists('message', $jsonResponse) ? $jsonResponse['message'] : '';
        $this->messageDetail = array_key_exists('messageDetail', $jsonResponse) ? $jsonResponse['messageDetail'] : null;
        $this->status = array_key_exists('status', $jsonResponse) ? $jsonResponse['status'] : 'error';
        $this->success = ! ($is_client_error || $is_server_error) && $this->status === 'success';
    }

    /**
     * Returns the raw HTTP response.
     */
    public function getResponseInterface(): ResponseInterface
    {
        return $this->response;
    }
}
