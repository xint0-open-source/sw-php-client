<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb\Helpers;

use DateTimeImmutable;
use PhpCfdi\Credentials\Credential;
use PhpCfdi\XmlCancelacion\Credentials;
use PhpCfdi\XmlCancelacion\Models\CancelDocuments;
use PhpCfdi\XmlCancelacion\XmlCancelacionHelper;

class CancellationRequestSigner
{
    /**
     * Creates a new instance of the
     * `\Xint0\SmarterWeb\Helpers\CancellationRequestSigner` class.
     *
     * @param CancelDocuments $documents Collection of documents to cancel.
     * @param DateTimeImmutable $timestamp Optional timestamp to use for request. Defaults to current time.
     */
    public function __construct(
        public CancelDocuments $documents,
        public DateTimeImmutable $timestamp = new DateTimeImmutable(),
    ) {
    }

    /**
     * Generates the signed cancellation request XML document with the
     * specified digital seal certificate.
     *
     * @param Credential $digitalSealCertificate Digital seal certificate used for signing the request.
     */
    public function sign(Credential $digitalSealCertificate): string
    {
        $helper = new XmlCancelacionHelper(Credentials::createWithPhpCfdiCredential($digitalSealCertificate));
        return $helper->signCancellationUuids($this->documents, $this->timestamp);
    }
}
