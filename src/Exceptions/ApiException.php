<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb\Exceptions;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\NetworkExceptionInterface;
use RuntimeException;

/**
 * Thrown when Api encounters an error.
 */
class ApiException extends RuntimeException
{
    public const CODE_REQUEST_NETWORK_EXCEPTION = 1;
    public const CODE_REQUEST_CLIENT_EXCEPTION = 2;

    /**
     * Creates Xint0\SmarterWeb\Exceptions\ApiException instance from request network exception.
     *
     * @param string $requestName Name of the request that failed.
     * @param NetworkExceptionInterface $previous The previous network exception.
     */
    public static function fromRequestNetworkException(string $requestName, NetworkExceptionInterface $previous): self
    {
        return new self(
            sprintf('Network issue occurred when processing %s request.', $requestName),
            self::CODE_REQUEST_NETWORK_EXCEPTION,
            $previous
        );
    }

    /**
     * Creates Xint0\SmarterWeb\Exceptions\ApiException instance from request client exception.
     *
     * @param string $requestName Name of the request that failed.
     * @param ClientExceptionInterface $previous The previous client exception.
     */
    public static function fromRequestClientException(string $requestName, ClientExceptionInterface $previous): self
    {
        return new self(
            sprintf('HTTP client error occurred when processing %s request.', $requestName),
            self::CODE_REQUEST_CLIENT_EXCEPTION,
            $previous
        );
    }
}
