<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

namespace Xint0\SmarterWeb\Enums;

enum Environment: string
{
    case PRODUCTION = 'production';
    case TESTING = 'testing';

    public function host(): string
    {
        return match ($this) {
            self::PRODUCTION => 'services.sw.com.mx',
            self::TESTING => 'services.test.sw.com.mx',
        };
    }
}
