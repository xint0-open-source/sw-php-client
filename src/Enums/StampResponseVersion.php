<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

namespace Xint0\SmarterWeb\Enums;

/**
 * The stamp response version.
 * {@see https://developers.sw.com.mx/knowledge-base/versiones-de-respuesta-timbrado/ Stamp response versions}.
 */
enum StampResponseVersion: string
{
    case VERSION_1 = 'v1';
    case VERSION_2 = 'v2';
    case VERSION_3 = 'v3';
    case VERSION_4 = 'v4';
}
