<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Message\StreamInterface;
use Xint0\SmarterWeb\Enums\Environment;
use Xint0\SmarterWeb\Enums\StampResponseVersion;
use Xint0\SmarterWeb\Exceptions\ApiException;
use Xint0\SmarterWeb\Factories\HttpClientFactory;
use Xint0\SmarterWeb\Factories\RequestFactory;

class Api
{
    private ClientInterface $client;
    private readonly RequestFactory $requestFactory;

    public function __construct(string $token, ?Environment $environment = null, ?ClientInterface $client = null)
    {
        $this->initializeClient($token, $client);
        $this->requestFactory = new RequestFactory($environment ?? Environment::PRODUCTION);
    }

    /**
     * Sends signed cancellation request to web service.
     *
     * @param  StreamInterface|resource|string  $xml  The signed cancellation request XML.
     *
     * @return CancelResponse
     *
     * @throws ApiException When error occurs while processing request.
     */
    public function cancel($xml): CancelResponse
    {
        $request = $this->requestFactory->makeCancelRequest($xml);
        try {
            return new CancelResponse($this->client->sendRequest($request));
        } catch (NetworkExceptionInterface $exception) {
            throw ApiException::fromRequestNetworkException('cancel', $exception);
        } catch (ClientExceptionInterface $exception) {
            throw ApiException::fromRequestClientException('cancel', $exception);
        }
    }

    /**
     * Sends signed fiscal voucher XML to web service for stamping.
     *
     * @param  StreamInterface|resource|string  $xml  The signed fiscal voucher XML.
     * @param  StampResponseVersion|null  $responseVersion  The response version.
     *
     * {@see https://developers.sw.com.mx/knowledge-base/versiones-de-respuesta-timbrado/ Stamp response version}.
     * Defaults to version 1.
     *
     * @return StampResponse
     *
     * @throws ApiException When error occurs while processing request.
     */
    public function stamp($xml, ?StampResponseVersion $responseVersion = null): StampResponse
    {
        $responseVersion ??= StampResponseVersion::VERSION_1;
        $request = $this->requestFactory->makeStampRequest($xml, $responseVersion);
        try {
            return new StampResponse($this->client->sendRequest($request));
        } catch (NetworkExceptionInterface $exception) {
            throw ApiException::fromRequestNetworkException('stamp', $exception);
        } catch (ClientExceptionInterface $exception) {
            throw ApiException::fromRequestClientException('stamp', $exception);
        }
    }

    private function initializeClient(string $token, ?ClientInterface $client): void
    {
        $httpClientFactory = new HttpClientFactory();
        $this->client = $httpClientFactory->create($token, $client);
    }
}
