<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb\Factories;

use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\Plugin\ContentLengthPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\Psr18ClientDiscovery;
use Http\Message\Authentication\Bearer;
use Psr\Http\Client\ClientInterface;

class HttpClientFactory
{
    /**
     * Returns an object that implements HTTP client interface, configured with
     * the specified authentication token.
     *
     * @param string $token Security token for authenticating requests.
     * @param ClientInterface|null $client Optional PSR-18 HTTP client.
     */
    public function create(
        string $token,
        ?ClientInterface $client = null
    ): ClientInterface {
        $plugins[] = new AuthenticationPlugin(new Bearer($token));
        $plugins[] = new ContentLengthPlugin();
        return new PluginClient($client ?? Psr18ClientDiscovery::find(), $plugins);
    }
}
