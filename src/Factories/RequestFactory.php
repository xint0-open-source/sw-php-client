<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Xint0\SmarterWeb\Factories;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use Xint0\SmarterWeb\Enums\Environment;
use Xint0\SmarterWeb\Enums\StampResponseVersion;

class RequestFactory
{
    private UriInterface $baseUri;
    private readonly MultipartStreamBuilder $multipartStreamBuilder;
    private readonly RequestFactoryInterface $requestFactory;

    public function __construct(Environment $environment = Environment::PRODUCTION)
    {
        $this->initBaseUri($environment);
        $this->requestFactory = Psr17FactoryDiscovery::findRequestFactory();
        $this->multipartStreamBuilder = new MultipartStreamBuilder();
    }

    /**
     * @param  StreamInterface|resource|string  $xml  The signed cancellation request XML.
     */
    public function makeCancelRequest($xml): RequestInterface
    {
        $uri = $this->baseUri->withPath('/cfdi33/cancel/xml');
        $this->multipartStreamBuilder->addResource('xml', $xml, ['headers' => [], 'filename' => 'cancel.xml']);
        $multipartStream = $this->multipartStreamBuilder->build();
        $boundary = $this->multipartStreamBuilder->getBoundary();
        return $this->requestFactory->createRequest('POST', $uri)
            ->withHeader('Content-Type', 'multipart/form-data; boundary="' . $boundary . '"')
            ->withBody($multipartStream);
    }

    /**
     * @param  StreamInterface|resource|string  $xml  The signed fiscal voucher XML.
     * @param StampResponseVersion $responseVersion The version of the response, defaults to version 1.
     *
     * Response version:
     * {@see https://developers.sw.com.mx/knowledge-base/versiones-de-respuesta-timbrado/ Stamp response version}.
     */
    public function makeStampRequest(
        $xml,
        StampResponseVersion $responseVersion = StampResponseVersion::VERSION_1,
    ): RequestInterface {
        $uri = $this->baseUri->withPath('/cfdi33/stamp/' . $responseVersion->value);
        $this->multipartStreamBuilder->addResource('xml', $xml, ['headers' => [], 'filename' => 'cfdi.xml']);
        $multipartStream = $this->multipartStreamBuilder->build();
        $boundary = $this->multipartStreamBuilder->getBoundary();
        return $this->requestFactory->createRequest('POST', $uri)
            ->withHeader('Content-Type', 'multipart/form-data; boundary="' . $boundary . '"')
            ->withBody($multipartStream);
    }

    private function initBaseUri(Environment $environment): void
    {
        $this->baseUri = Psr17FactoryDiscovery::findUriFactory()
            ->createUri()
            ->withScheme('https')
            ->withHost($environment->host());
    }
}
