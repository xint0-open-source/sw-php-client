# SmarterWeb PHP client

## Description

SmarterWeb fiscal voucher stamping and cancellation web service client.

## Installation

Quick installation:

```bash
composer require xint0/sw-php-client
```

The SmarterWeb PHP client requires a PSR-18 HTTP client that
provides [psr/http-client-implementation](https://packagist.org/providers/psr/http-client-implementation),
and uses `php-http/discovery` package to find, and install an implementation if one is not already installed.

## Usage

Once you have a token for using the SmarterWeb web services you can create an instance of the `Xint0\SmarterWeb\Api`
class:

### Stamp

#### Stamping signed fiscal voucher from string read from file

```php
use Xint0\SmarterWeb\Api;
use Xint0\SmarterWeb\Exceptions\ApiException;

$token = 'your_smarterweb_security_token';
$api = new Api($token);

$xml = file_get_contents('path_to_signed_fiscal_voucher.xml');

try {
    /*
     * Calls stamp method requesting version 1 response
     * see https://developers.sw.com.mx/knowledge-base/versiones-de-respuesta-timbrado/
     */
    $response = $api->stamp($xml);

    if ($response->success) {
        $fiscal_voucher_stamp_xml_fragment = $response->data['tfd'];
    } else {
        /**
         * In case service returns an error response more information is available
         * in message and messageDetail properties.
         * @see https://developers.sw.com.mx/knowledge-base/cancelacion-cfdi/
         */
        $message = $response->message;
        $messageDetail = $response->messageDetail;
    }
} catch (ApiException $exception) {
    /**
     * In case an error occurred when sending the request or receiving the response
     * an ApiException is thrown
     */
    $message = $exception->getMessage();
}
```

### Cancellation

#### Request fiscal voucher cancellation using file resource of signed cancellation request XML

```php
use Xint0\SmarterWeb\Api;
use Xint0\SmarterWeb\Exceptions\ApiException;

$token = 'your_smarterweb_security_token';
$api = new Api($token);

$cancellation_request_xml = fopen('path_to_signed_cancellation_request.xml');

try {
    $response = $api->cancel($cancellation_request_xml);

    if ($response->success) {
        $data = $response->data;
        /**
         * The response data array has two keys: acuse and uuid.
         * The acuse key value is the cancellation request acknowledgement XML.
         * The uuid key value is and array with each of the fiscal voucher folios
         * included in the request as key and the cancellation request code as value.
         * @see https://developers.sw.com.mx/knowledge-base/cancelacion-cfdi/
         */
        $fiscal_voucher_cancellation_request_acknowledgement_xml = $data['acuse'];
        $individual_fiscal_voucher_cancellation_request_code = $data['uuid']['3EAEABC9-EA41-4627-9609-C6856B78E2B1'];
    } else {
        /**
         * In case service returns an error response more information is available
         * in message and messageDetail properties.
         * @see https://developers.sw.com.mx/knowledge-base/cancelacion-cfdi/
         */
        $message = $response->message;
        $messageDetail = $response->messageDetail;
    }
} catch (ApiException $exception) {
    /**
     * In case an error occurred when sending the request or receiving the response
     * an ApiException is thrown
     */
    $message = $exception->getMessage();
} finally {
    fclose($cancellation_request_xml);
}
```

## Support

If you need assistance you can open an issue at: https://gitlab.com/xint0-open-source/sw-php-client/-/issues

## Roadmap

| Method | Is implemented |
|--------|----------------|
| stamp  | Yes            |
| cancel | Yes            |

## Contributing

You can fork the project and submit a merge request.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

This component is open source licensed under the MIT license.
