# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.3.1...main)

### Changed

- Upgrade `phpcs` and `phpcbf` to 3.11.1.
- Set phpstan level to 9.
- Add php 8.4 to build and test matrix.

### Fixed

- Set default environment when creating request factory in Api constructor.

## [0.3.1](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.3.0...0.3.1) - 2024-09-02

### Fixed

- Pass `headers` option to multipart stream builder `addResource` method.

## [0.3.0](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.2.1...0.3.0) - 2024-07-06

### Changed

- Do not accept `null` in `$responseVersion` and default to `StampResponseVersion::VERSION_1` in
  `Xint0\SmarterWeb\Factories\RequestFactory::makeStampRequest`.
- Do not accept `null` in `$environment` and default to `Environment::PRODUCTION` in
  `Xint0\SmarterWeb\Factories\RequestFactory::__construct`.
- Update README installation section and add examples titles with context.

## [0.2.1](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.2.0...0.2.1) - 2024-06-27

### Changed

- Drop support for PHP 8.0, set minimum PHP supported version to 8.1.
- Add PHP 8.3 to CI pipeline
- Upgrade PHPUnit to 10.5.
- Convert `Environment` and `StampResponseVersion` to native enums.
- Remove `myclabs/php-enum` from dependencies.
- Support `psr/http-message` ^1.0 || ^2.0.
- Support `psr/log` ^2.0 || ^ 3.0.
- Use PHPUnit attributes instead of PHPDoc annotations.

## [0.2.0](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.1.1...0.2.0) - 2023-11-15

- Upgrade `psr/log` to version 2.0.

## [0.1.1](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.1.0...0.1.1) - 2023-08-06

### Added

- Support for specifying the response version for the `Api@stamp` method.
  See [Stamp response versions](https://developers.sw.com.mx/knowledge-base/versiones-de-respuesta-timbrado/).

## [0.1.0](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.0.3...0.1.0) - 2023-08-04

### Changed

- Drop support for PHP 7.4.
- Add PHPDoc annotations.

## [0.0.3](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.0.2...0.0.3) - 2023-03-26

### Changed

- Add `php-http/client-common` as direct dependency.
- Remove `xint0/credential-storage-contract` dependency.
- Remove `replaces` key from composer.json.
- Add `support` key to composer.json with `source`, `issues`, and `email` values.

## [0.0.2](https://gitlab.com/xint0-open-source/sw-php-client/-/compare/0.0.1...0.0.2) - 2023-03-26

### Added

- `Api@cancel` method.

### Changed

- Include `Api@cancel` example in README.
- Updated `Api@stamp` example in README.

## [0.0.1](https://gitlab.com/xint0-open-source/sw-php-client/-/tags/0.0.1) - 2023-03-19

### Added

- `Api@stamp` method.
