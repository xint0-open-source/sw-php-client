<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Exceptions;

use Http\Client\Exception\NetworkException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;
use Xint0\SmarterWeb\Exceptions\ApiException;

#[CoversClass(ApiException::class)]
class ApiExceptionTest extends TestCase
{
    public function test_from_request_network_exception_returns_instance_with_expected_message(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(NetworkException::class);
        $actual = ApiException::fromRequestNetworkException('stamp', $previous);
        $this->assertSame('Network issue occurred when processing stamp request.', $actual->getMessage());
    }

    public function test_from_request_network_exception_returns_instance_with_expected_code(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(NetworkException::class);
        $actual = ApiException::fromRequestNetworkException('stamp', $previous);
        $this->assertSame(ApiException::CODE_REQUEST_NETWORK_EXCEPTION, $actual->getCode());
    }

    public function test_from_request_network_exception_returns_instance_with_expected_previous_exception(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(NetworkException::class);
        $actual = ApiException::fromRequestNetworkException('stamp', $previous);
        $this->assertSame($previous, $actual->getPrevious());
    }

    public function test_from_request_client_exception_returns_instance_with_expected_message(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(ClientExceptionInterface::class);
        $actual = ApiException::fromRequestClientException('stamp', $previous);
        $this->assertSame('HTTP client error occurred when processing stamp request.', $actual->getMessage());
    }

    public function test_from_request_client_exception_returns_instance_with_expected_code(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(ClientExceptionInterface::class);
        $actual = ApiException::fromRequestClientException('stamp', $previous);
        $this->assertSame(ApiException::CODE_REQUEST_CLIENT_EXCEPTION, $actual->getCode());
    }

    public function test_from_request_client_exception_returns_instance_with_expected_previous_exception(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $previous = $this->createStub(ClientExceptionInterface::class);
        $actual = ApiException::fromRequestClientException('stamp', $previous);
        $this->assertSame($previous, $actual->getPrevious());
    }
}
