<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Helpers;

use DateTimeImmutable;
use PhpCfdi\Credentials\Credential;
use PhpCfdi\XmlCancelacion\Models\CancelDocument;
use PhpCfdi\XmlCancelacion\Models\CancelDocuments;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Tests\Concerns\UsesTestFixtures;
use Xint0\SmarterWeb\Helpers\CancellationRequestSigner;

#[CoversClass(CancellationRequestSigner::class)]
class CancellationRequestSignerTest extends TestCase
{
    use UsesTestFixtures;

    public function test_new_instance_has_expected_properties_when_timestamp_is_not_specified(): void
    {
        $documents = new CancelDocuments(
            CancelDocument::newWithErrorsUnrelated('62B00C5E-4187-4336-B569-44E0030DC729')
        );
        $now = new DateTimeImmutable();
        $actual = new CancellationRequestSigner($documents);

        $this->assertSame($documents, $actual->documents);
        $this->assertEquals($now->format(DATE_ATOM), $actual->timestamp->format(DATE_ATOM));
    }

    public function test_new_instance_has_expected_properties_when_timestamp_is_specified(): void
    {
        $documents = new CancelDocuments(
            CancelDocument::newWithErrorsUnrelated('62B00C5E-4187-4336-B569-44E0030DC729')
        );
        $actual = new CancellationRequestSigner($documents, new DateTimeImmutable('2022-01-06 17:49:12'));

        $this->assertSame($documents, $actual->documents);
        $this->assertEquals('2022-01-06T17:49:12+00:00', $actual->timestamp->format(DATE_ATOM));
    }

    public function test_sign_method_returns_expected_xml(): void
    {
        $documents = new CancelDocuments(
            CancelDocument::newWithErrorsUnrelated('62B00C5E-4187-4336-B569-44E0030DC729')
        );
        $sut = new CancellationRequestSigner($documents, new DateTimeImmutable('2022-01-06 17:49:12'));

        $actual = $sut->sign($this->getDigitalSealCertificate());
        $this->assertXmlStringEqualsXmlFile($this->getFixtureFullPath('cancellation_request.xml'), $actual);
    }

    private function getDigitalSealCertificate(): Credential
    {
        return Credential::openFiles(
            $this->getFixtureFullPath('certs/EKU9003173C9.cer'),
            $this->getFixtureFullPath('certs/EKU9003173C9.key.pem'),
            trim($this->getFixtureContents('certs/EKU9003173C9.password.bin'))
        );
    }
}
