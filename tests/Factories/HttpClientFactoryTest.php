<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories;

use Http\Client\Common\PluginClient;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Xint0\SmarterWeb\Factories\HttpClientFactory;

#[CoversClass(HttpClientFactory::class)]
class HttpClientFactoryTest extends TestCase
{
    public function test_create_method_returns_instance_that_sends_requests_with_expected_authorization_header(): void
    {
        $mockClient = new Client();
        $sut = new HttpClientFactory();
        $client = $sut->create('token', $mockClient);
        $testRequest = Psr17FactoryDiscovery::findRequestFactory()->createRequest('POST', 'https://example.com');
        /** @noinspection PhpUnhandledExceptionInspection */
        $client->sendRequest($testRequest);
        /** @var RequestInterface $lastRequest */
        $lastRequest = $mockClient->getLastRequest();
        $this->assertSame(['Bearer token'], $lastRequest->getHeader('authorization'));
    }

    public function test_create_method_returns_instance_that_sends_requests_with_content_length_header(): void
    {
        $mockClient = new Client();
        $sut = new HttpClientFactory();
        $client = $sut->create('token', $mockClient);
        $testRequest = Psr17FactoryDiscovery::findRequestFactory()
            ->createRequest('POST', 'https://example.com')
            ->withBody(Psr17FactoryDiscovery::findStreamFactory()->createStream('TEST'));
        /** @noinspection PhpUnhandledExceptionInspection */
        $client->sendRequest($testRequest);
        /** @var RequestInterface $lastRequest */
        $lastRequest = $mockClient->getLastRequest();
        $this->assertArrayHasKey('Content-Length', $lastRequest->getHeaders());
        $this->assertSame(['4'], $lastRequest->getHeader('content-length'));
    }

    public function test_create_method_returns_instance_when_client_is_not_specified(): void
    {
        $original_strategies = self::currentPsr18ClientDiscoveryStrategies();
        Psr18ClientDiscovery::prependStrategy(MockClientStrategy::class);
        try {
            $sut = new HttpClientFactory();
            $actual = $sut->create('token');
            $this->assertInstanceOf(PluginClient::class, $actual);
        } finally {
            Psr18ClientDiscovery::setStrategies($original_strategies);
        }
    }

    /**
     * @return string[]
     */
    private static function currentPsr18ClientDiscoveryStrategies(): array
    {
        $result = [];
        foreach (Psr18ClientDiscovery::getStrategies() as $strategy) {
            $result[] = $strategy;
        }
        return $result;
    }
}
