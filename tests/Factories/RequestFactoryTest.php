<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Factories;

use Generator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use Tests\Concerns\UsesTestFixtures;
use Xint0\SmarterWeb\Enums\Environment;
use Xint0\SmarterWeb\Enums\StampResponseVersion;
use Xint0\SmarterWeb\Factories\RequestFactory;

#[CoversClass(RequestFactory::class)]
#[UsesClass(Environment::class)]
#[UsesClass(StampResponseVersion::class)]
class RequestFactoryTest extends TestCase
{
    use UsesTestFixtures;

    public static function environmentHostProvider(): Generator
    {
        foreach (Environment::cases() as $environment) {
            yield $environment->name => [$environment, $environment->host()];
        }
    }

    /**
     * @return array<string,array{0:StampResponseVersion|null, 1:string}>
     */
    public static function responseVersionPathProvider(): array
    {
        return [
            'unspecified' => [
                null,
                '/cfdi33/stamp/v1',
            ],
            'version 1' => [
                StampResponseVersion::VERSION_1,
                '/cfdi33/stamp/v1',
            ],
            'version 2' => [
                StampResponseVersion::VERSION_2,
                '/cfdi33/stamp/v2',
            ],
            'version 3' => [
                StampResponseVersion::VERSION_3,
                '/cfdi33/stamp/v3',
            ],
            'version 4' => [
                StampResponseVersion::VERSION_4,
                '/cfdi33/stamp/v4',
            ],
        ];
    }

    #[DataProvider('environmentHostProvider')]
    public function test_make_stamp_request_returns_request_interface_with_expected_method_and_uri_depending_on_environment(
        Environment $environment,
        string $expected_host
    ): void {
        $xml = '<cfdi:Comprobante/>';
        $sut = new RequestFactory($environment);
        $actual = $sut->makeStampRequest($xml);
        $this->assertSame('POST', $actual->getMethod());
        $this->assertSame('https', $actual->getUri()->getScheme());
        $this->assertSame($expected_host, $actual->getUri()->getHost());
        $this->assertSame('/cfdi33/stamp/v1', $actual->getUri()->getPath());
    }

    #[DataProvider('responseVersionPathProvider')]
    public function test_make_stamp_request_returns_request_interface_with_expected_path_depending_on_response_version(?StampResponseVersion $responseVersion, string $expected_path): void
    {
        $xml = '<cfdi:Comprobante/>';
        $sut = new RequestFactory(Environment::TESTING);
        $params = array_filter([$xml, $responseVersion]);
        $actual = $sut->makeStampRequest(...$params);
        $this->assertSame($expected_path, $actual->getUri()->getPath());
    }

    public function test_make_stamp_request_returns_request_interface_without_authorization_header(): void
    {
        $xml = '<cfdi:Comprobante/>';
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeStampRequest($xml);
        $this->assertArrayNotHasKey('Authorization', $actual->getHeaders());
    }

    public function test_make_stamp_request_returns_request_interface_with_expected_content_type_header(): void
    {
        $xml = '<cfdi:Comprobante/>';
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeStampRequest($xml);
        $this->assertArrayHasKey('Content-Type', $actual->getHeaders());
        $this->assertStringStartsWith('multipart/form-data; boundary="', $actual->getHeaderLine('content-type'));
    }

    public function test_make_stamp_request_returns_request_interface_with_expected_body(): void
    {
        $xml = '<cfdi:Comprobante/>';
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeStampRequest($xml);
        $matches = [];
        preg_match('/boundary="([^"]*)"$/', $actual->getHeaderLine('Content-Type'), $matches);
        if (count($matches) <= 1) {
            self::fail('Could not determine boundary string.');
        }
        $boundary = $matches[1];
        $this->assertNotEmpty($boundary);
        $expectedBody = "--$boundary\r\n" .
            "Content-Disposition: form-data; name=\"xml\"; filename=\"cfdi.xml\"\r\n" .
            "Content-Type: application/xml\r\n" .
            "\r\n" .
            $xml . "\r\n" .
            "--$boundary--\r\n";
        $this->assertSame($expectedBody, $actual->getBody()->getContents());
    }

    #[DataProvider('environmentHostProvider')]
    public function test_make_stamp_request_returns_request_interface_with_expected_method_and_uri_depending_on_environment_using_a_resource(
        Environment $environment,
        string $expected_host
    ): void {
        if (false === $xmlResource = fopen($this->getFixtureFullPath('mock_cfdi.xml'), 'r')) {
            self::fail('Could not open mock_cfdi.xml for reading.');
        }
        try {
            $sut = new RequestFactory($environment);
            $actual = $sut->makeStampRequest($xmlResource);
            $this->assertSame('POST', $actual->getMethod());
            $this->assertSame('https', $actual->getUri()->getScheme());
            $this->assertSame($expected_host, $actual->getUri()->getHost());
            $this->assertSame('/cfdi33/stamp/v1', $actual->getUri()->getPath());
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_make_stamp_request_returns_request_interface_with_expected_headers(): void
    {
        if (false === $xmlResource = fopen($this->getFixtureFullPath('mock_cfdi.xml'), 'r')) {
            self::fail('Could not open mock_cfdi.xml for reading.');
        }
        try {
            $sut = new RequestFactory(Environment::TESTING);
            $actual = $sut->makeStampRequest($xmlResource);
            $this->assertArrayNotHasKey('Authorization', $actual->getHeaders());
            $this->assertArrayHasKey('Content-Type', $actual->getHeaders());
            $this->assertStringStartsWith('multipart/form-data; boundary="', $actual->getHeaderLine('content-type'));
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_make_stamp_request_returns_request_interface_with_expected_body_using_resource(): void
    {
        $xmlContents = $this->getFixtureContents('mock_cfdi.xml');
        if (false === $xmlResource = fopen($this->getFixtureFullPath('mock_cfdi.xml'), 'r')) {
            self::fail('Could not open mock_cfdi.xml for reading.');
        }
        try {
            $sut = new RequestFactory(Environment::TESTING);
            $actual = $sut->makeStampRequest($xmlResource);
            $matches = [];
            preg_match('/boundary="([^"]*)"$/', $actual->getHeaderLine('Content-Type'), $matches);
            if (count($matches) <= 1) {
                self::fail('Could not determine boundary string.');
            }
            $boundary = $matches[1];
            $this->assertNotEmpty($boundary);
            $expectedBody = "--$boundary\r\n"
                . "Content-Disposition: form-data; name=\"xml\"; filename=\"cfdi.xml\"\r\n"
                . "Content-Type: application/xml\r\n"
                . "\r\n"
                . $xmlContents . "\r\n"
                . "--$boundary--\r\n";
            $this->assertSame($expectedBody, $actual->getBody()->getContents());
        } finally {
            fclose($xmlResource);
        }
    }

    #[DataProvider('environmentHostProvider')]
    public function test_make_cancel_request_returns_request_interface_with_expected_method_and_uri_depending_on_environment(
        Environment $environment,
        string $expected_host
    ): void {
        $xml = $this->getFixtureContents('cancellation_request.xml');
        $sut = new RequestFactory($environment);
        $actual = $sut->makeCancelRequest($xml);
        $this->assertSame('POST', $actual->getMethod());
        $this->assertSame('https', $actual->getUri()->getScheme());
        $this->assertSame($expected_host, $actual->getUri()->getHost());
        $this->assertSame('/cfdi33/cancel/xml', $actual->getUri()->getPath());
    }

    public function test_make_cancel_request_returns_request_interface_without_authorization_header(): void
    {
        $xml = $this->getFixtureContents('cancellation_request.xml');
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeCancelRequest($xml);
        $this->assertArrayNotHasKey('Authorization', $actual->getHeaders());
    }

    public function test_make_cancel_request_returns_request_interface_with_expected_content_type_header(): void
    {
        $xml = $this->getFixtureContents('cancellation_request.xml');
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeCancelRequest($xml);
        $this->assertMatchesRegularExpression(
            '/^multipart\/form-data; boundary=".+"$/',
            $actual->getHeaderLine('content-type')
        );
    }

    public function test_make_cancel_request_returns_request_with_expected_body(): void
    {
        $xml = $this->getFixtureContents('cancellation_request.xml');
        $sut = new RequestFactory(Environment::TESTING);
        $actual = $sut->makeCancelRequest($xml);
        $matches = [];
        preg_match('/boundary="([^"]*)"$/', $actual->getHeaderLine('Content-Type'), $matches);
        if (count($matches) <= 1) {
            self::fail('Could not determine boundary string.');
        }
        $boundary = $matches[1];
        $expectedBody = "--$boundary\r\n" .
            "Content-Disposition: form-data; name=\"xml\"; filename=\"cancel.xml\"\r\n" .
            "Content-Type: application/xml\r\n" .
            "\r\n" .
            $xml . "\r\n" .
            "--$boundary--\r\n";
        $this->assertSame($expectedBody, $actual->getBody()->getContents());
    }

    #[DataProvider('environmentHostProvider')]
    public function test_make_cancel_request_returns_request_interface_with_expected_method_and_uri_depending_on_environment_using_a_resource(
        Environment $environment,
        string $expected_host
    ): void {
        if (false === $xmlResource = fopen($this->getFixtureFullPath('cancellation_request.xml'), 'r')) {
            self::fail('Could not open cancellation_request.xml for reading.');
        }
        try {
            $sut = new RequestFactory($environment);
            $actual = $sut->makeCancelRequest($xmlResource);
            $this->assertSame('POST', $actual->getMethod());
            $this->assertSame('https', $actual->getUri()->getScheme());
            $this->assertSame($expected_host, $actual->getUri()->getHost());
            $this->assertSame('/cfdi33/cancel/xml', $actual->getUri()->getPath());
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_make_cancel_request_returns_request_interface_with_expected_headers_using_a_resource(): void
    {
        if (false === $xmlResource = fopen($this->getFixtureFullPath('cancellation_request.xml'), 'r')) {
            self::fail('Could not open cancellation_request.xml for reading.');
        }
        try {
            $sut = new RequestFactory(Environment::TESTING);
            $actual = $sut->makeCancelRequest($xmlResource);
            $this->assertArrayNotHasKey('Authorization', $actual->getHeaders());
            $this->assertArrayHasKey('Content-Type', $actual->getHeaders());
            $this->assertMatchesRegularExpression(
                '/^multipart\/form-data; boundary=".+"$/',
                $actual->getHeaderLine('content-type')
            );
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_make_cancel_request_returns_request_interface_with_expected_body_using_a_resource(): void
    {
        $xmlContents = $this->getFixtureContents('cancellation_request.xml');
        if (false === $xmlResource = fopen($this->getFixtureFullPath('cancellation_request.xml'), 'r')) {
            self::fail('Could not open cancellation_request.xml for reading.');
        }
        try {
            $sut = new RequestFactory(Environment::TESTING);
            $actual = $sut->makeCancelRequest($xmlResource);
            $matches = [];
            preg_match('/boundary="([^"]*)"$/', $actual->getHeaderLine('content-type'), $matches);
            if (count($matches) <= 1) {
                self::fail('Could not determine boundary string.');
            }
            $boundary = $matches[1];
            $this->assertNotEmpty($boundary);
            $expectedBody = "--$boundary\r\n" .
                "Content-Disposition: form-data; name=\"xml\"; filename=\"cancel.xml\"\r\n" .
                "Content-Type: application/xml\r\n" .
                "\r\n" .
                $xmlContents . "\r\n" .
                "--$boundary--\r\n";
            $this->assertSame($expectedBody, $actual->getBody()->getContents());
        } finally {
            fclose($xmlResource);
        }
    }
}
