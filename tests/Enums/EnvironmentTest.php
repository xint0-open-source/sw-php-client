<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Enums;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Xint0\SmarterWeb\Enums\Environment;

#[CoversClass(Environment::class)]
class EnvironmentTest extends TestCase
{
    /**
     * @return array<string,array<int,Environment|string>>
     */
    public static function expectedHostProvider(): array
    {
        return [
            'production' => [
                Environment::PRODUCTION,
                'services.sw.com.mx',
            ],
            'testing' => [
                Environment::TESTING,
                'services.test.sw.com.mx',
            ],
        ];
    }

    #[DataProvider('expectedHostProvider')]
    public function test_host_method_returns_expected_value(Environment $sut, string $expected_host): void
    {
        $this->assertSame($expected_host, $sut->host());
    }
}
