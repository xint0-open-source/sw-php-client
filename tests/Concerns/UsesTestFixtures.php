<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests\Concerns;

use PHPUnit\Framework\TestCase;

/**
 * @mixin TestCase
 */
trait UsesTestFixtures
{
    protected string $fixtures_directory = 'fixtures';

    public function getFixtureContents(string $fixturePath, ?string $fixtureDirectory = null): string
    {
        $fullPath = $this->getFixtureFullPath($fixturePath, $fixtureDirectory);
        $contents = file_get_contents($fullPath);
        if ($contents === false) {
            $this->fail("Could not read fixture $fixturePath!");
        }

        return $contents;
    }

    public function getFixtureFullPath(string $fixturePath, ?string $fixtureDirectory = null): string
    {
        $fixtureDirectory ??= $this->fixtures_directory;
        $fullPath = realpath(
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $fixtureDirectory . DIRECTORY_SEPARATOR . $fixturePath
        );
        if (false === $fullPath) {
            $this->fail("Fixture $fixturePath not found!");
        }

        return $fullPath;
    }
}
