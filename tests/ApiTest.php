<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests;

use Http\Client\Exception\NetworkException;
use Http\Client\Exception\TransferException;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Mock\Client;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\UsesClass;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Tests\Concerns\UsesTestFixtures;
use Throwable;
use Xint0\SmarterWeb\Api;
use Xint0\SmarterWeb\CancelResponse;
use Xint0\SmarterWeb\Enums\Environment;
use Xint0\SmarterWeb\Enums\StampResponseVersion;
use Xint0\SmarterWeb\Exceptions\ApiException;
use Xint0\SmarterWeb\Factories\HttpClientFactory;
use Xint0\SmarterWeb\Factories\RequestFactory;
use Xint0\SmarterWeb\StampResponse;

#[CoversClass(Api::class)]
#[UsesClass(ApiException::class)]
#[UsesClass(CancelResponse::class)]
#[UsesClass(Environment::class)]
#[UsesClass(HttpClientFactory::class)]
#[UsesClass(RequestFactory::class)]
#[UsesClass(StampResponse::class)]
#[UsesClass(StampResponseVersion::class)]
class ApiTest extends TestCase
{
    use UsesTestFixtures;

    /**
     * @return array<string,array{responseVersion: StampResponseVersion|null, expected_path: string}>
     */
    public static function stampResponseVersionPathProvider(): array
    {
        return [
            'unspecified' => [
                'responseVersion' => null,
                'expected_path' => '/cfdi33/stamp/v1',
            ],
            'version 1' => [
                'responseVersion' => StampResponseVersion::VERSION_1,
                'expected_path' => '/cfdi33/stamp/v1',
            ],
            'version 2' => [
                'responseVersion' => StampResponseVersion::VERSION_2,
                'expected_path' => '/cfdi33/stamp/v2',
            ],
            'version 3' => [
                'responseVersion' => StampResponseVersion::VERSION_3,
                'expected_path' => '/cfdi33/stamp/v3',
            ],
            'version 4' => [
                'responseVersion' => StampResponseVersion::VERSION_4,
                'expected_path' => '/cfdi33/stamp/v4',
            ],
        ];
    }

    #[DataProvider('stampResponseVersionPathProvider')]
    public function test_stamp_method_posts_request_to_expected_endpoint_depending_on_stamp_response_version(?StampResponseVersion $responseVersion, string $expected_path): void
    {
        $mockHttpClient = new Client();
        $sut = new Api('token', Environment::TESTING, $mockHttpClient);
        $xmlContent = $this->getFixtureContents('mock_cfdi.xml');
        if (false === $xmlResource = fopen($this->getFixtureFullPath('mock_cfdi.xml'), 'r')) {
            self::fail('Could not open mock_cfdi.xml.');
        }
        try {
            $sut->stamp($xmlResource, $responseVersion);
            $this->assertCount(1, $mockHttpClient->getRequests());
            /** @var RequestInterface $lastRequest */
            $lastRequest = $mockHttpClient->getLastRequest();
            $this->assertSame('POST', $lastRequest->getMethod());
            $this->assertSame('https', $lastRequest->getUri()->getScheme());
            $this->assertSame(Environment::TESTING->host(), $lastRequest->getUri()->getHost());
            $this->assertSame($expected_path, $lastRequest->getUri()->getPath());
            $this->assertSame('Bearer token', $lastRequest->getHeaderLine('Authorization'));
            $this->assertStringContainsString($xmlContent, $lastRequest->getBody()->getContents());
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_stamp_method_posts_request_to_expected_endpoint_with_expected_data_using_resource(): void
    {
        $mockHttpClient = new Client();
        $sut = new Api('token', Environment::TESTING, $mockHttpClient);
        $xmlContent = $this->getFixtureContents('mock_cfdi.xml');
        if (false === $xmlResource = fopen($this->getFixtureFullPath('mock_cfdi.xml'), 'r')) {
            self::fail('Could not open mock_cfdi.xml.');
        }
        try {
            $sut->stamp($xmlResource);
            $this->assertCount(1, $mockHttpClient->getRequests());
            /** @var RequestInterface $lastRequest */
            $lastRequest = $mockHttpClient->getLastRequest();
            $this->assertSame('POST', $lastRequest->getMethod());
            $this->assertSame('https', $lastRequest->getUri()->getScheme());
            $this->assertSame(Environment::TESTING->host(), $lastRequest->getUri()->getHost());
            $this->assertSame('/cfdi33/stamp/v1', $lastRequest->getUri()->getPath());
            $this->assertSame('Bearer token', $lastRequest->getHeaderLine('Authorization'));
            $this->assertStringContainsString($xmlContent, $lastRequest->getBody()->getContents());
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_stamp_method_posts_request_to_expected_endpoint_with_expected_data_using_string(): void
    {
        $mockHttpClient = new Client();
        $sut = new Api('token', Environment::TESTING, $mockHttpClient);
        $xmlContent = $this->getFixtureContents('mock_cfdi.xml');
        $sut->stamp($xmlContent);
        $this->assertCount(1, $mockHttpClient->getRequests());
        /** @var RequestInterface $lastRequest */
        $lastRequest = $mockHttpClient->getLastRequest();
        $this->assertSame('POST', $lastRequest->getMethod());
        $this->assertSame('https', $lastRequest->getUri()->getScheme());
        $this->assertSame(Environment::TESTING->host(), $lastRequest->getUri()->getHost());
        $this->assertSame('/cfdi33/stamp/v1', $lastRequest->getUri()->getPath());
        $this->assertSame('Bearer token', $lastRequest->getHeaderLine('Authorization'));
        $this->assertStringContainsString($xmlContent, $lastRequest->getBody()->getContents());
    }

    public function test_stamp_method_throws_api_exception_when_client_throws_network_exception_interface(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $networkException = $this->createStub(NetworkException::class);
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException($networkException);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Network issue occurred when processing stamp request.');
        $this->expectExceptionCode(ApiException::CODE_REQUEST_NETWORK_EXCEPTION);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        try {
            $sut->stamp('<cfdi:Comprobante/>');
        } catch (Throwable $exception) {
            $this->assertSame($networkException, $exception->getPrevious());
            throw $exception;
        }
    }

    public function test_stamp_method_throws_api_exception_when_client_throws_client_exception_interface(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $clientException = $this->createStub(TransferException::class);
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException($clientException);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('HTTP client error occurred when processing stamp request.');
        $this->expectExceptionCode(ApiException::CODE_REQUEST_CLIENT_EXCEPTION);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        try {
            $sut->stamp('<cfdi:Comprobante/>');
        } catch (Throwable $exception) {
            $this->assertSame($clientException, $exception->getPrevious());
            throw $exception;
        }
    }

    public function test_stamp_method_returns_stamp_response_with_client_error_code(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $clientErrorResponse = Psr17FactoryDiscovery::findResponseFactory()->createResponse(400)
            ->withBody(
                Psr17FactoryDiscovery::findStreamFactory()->createStream(
                    json_encode([
                        'message' => '301 - La estructura del comprobante es incorrecta.',
                        'messageDetail' => null,
                        'data' => null,
                        'status' => 'error',
                    ], JSON_THROW_ON_ERROR)
                )
            );
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultResponse($clientErrorResponse);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        $response = $sut->stamp('<cfdi:Comprobante/>');

        $this->assertFalse($response->success);
        $this->assertSame('301 - La estructura del comprobante es incorrecta.', $response->message);
        $this->assertSame('error', $response->status);
    }

    public function test_cancel_method_posts_request_to_expected_endpoint_with_expected_data_using_resource(): void
    {
        $mockHttpClient = new Client();
        $sut = new Api('token', Environment::TESTING, $mockHttpClient);
        $xmlContent = $this->getFixtureContents('cancellation_request.xml');
        if (false === $xmlResource = fopen($this->getFixtureFullPath('cancellation_request.xml'), 'r')) {
            self::fail('Could not open cancellation_request.xml.');
        }
        try {
            $sut->cancel($xmlResource);
            $this->assertCount(1, $mockHttpClient->getRequests());
            /** @var RequestInterface $lastRequest */
            $lastRequest = $mockHttpClient->getLastRequest();
            $this->assertSame('POST', $lastRequest->getMethod());
            $this->assertSame('https', $lastRequest->getUri()->getScheme());
            $this->assertSame(Environment::TESTING->host(), $lastRequest->getUri()->getHost());
            $this->assertSame('/cfdi33/cancel/xml', $lastRequest->getUri()->getPath());
            $this->assertSame('Bearer token', $lastRequest->getHeaderLine('Authorization'));
            $this->assertStringContainsString($xmlContent, $lastRequest->getBody()->getContents());
        } finally {
            fclose($xmlResource);
        }
    }

    public function test_cancel_method_posts_request_to_expected_endpoint_with_expected_data_using_string(): void
    {
        $mockHttpClient = new Client();
        $sut = new Api('token', Environment::TESTING, $mockHttpClient);
        $xmlContent = $this->getFixtureContents('cancellation_request.xml');
        $sut->cancel($xmlContent);
        $this->assertCount(1, $mockHttpClient->getRequests());
        /** @var RequestInterface $lastRequest */
        $lastRequest = $mockHttpClient->getLastRequest();
        $this->assertSame('POST', $lastRequest->getMethod());
        $this->assertSame('https', $lastRequest->getUri()->getScheme());
        $this->assertSame(Environment::TESTING->host(), $lastRequest->getUri()->getHost());
        $this->assertSame('/cfdi33/cancel/xml', $lastRequest->getUri()->getPath());
        $this->assertSame('Bearer token', $lastRequest->getHeaderLine('Authorization'));
        $this->assertStringContainsString($xmlContent, $lastRequest->getBody()->getContents());
    }

    public function test_cancel_method_throws_api_exception_when_client_throws_network_exception_interface(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $networkException = $this->createStub(NetworkException::class);
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException($networkException);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('Network issue occurred when processing cancel request.');
        $this->expectExceptionCode(ApiException::CODE_REQUEST_NETWORK_EXCEPTION);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        try {
            $sut->cancel($this->getFixtureContents('cancellation_request.xml'));
        } catch (Throwable $exception) {
            $this->assertSame($networkException, $exception->getPrevious());
            /** @noinspection PhpUnhandledExceptionInspection */
            throw $exception;
        }
    }

    public function test_cancel_method_throws_api_exception_when_client_throws_client_exception_interface(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $clientException = $this->createStub(TransferException::class);
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultException($clientException);

        $this->expectException(ApiException::class);
        $this->expectExceptionMessage('HTTP client error occurred when processing cancel request.');
        $this->expectExceptionCode(ApiException::CODE_REQUEST_CLIENT_EXCEPTION);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        try {
            $sut->cancel($this->getFixtureContents('cancellation_request.xml'));
        } catch (Throwable $exception) {
            $this->assertSame($clientException, $exception->getPrevious());
            /** @noinspection PhpUnhandledExceptionInspection */
            throw $exception;
        }
    }

    public function test_cancel_method_returns_cancel_response_with_client_error_code(): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $clientErrorResponse = Psr17FactoryDiscovery::findResponseFactory()->createResponse(400)
            ->withBody(
                Psr17FactoryDiscovery::findStreamFactory()->createStream(
                    json_encode([
                        'message' => 'CACFDI33 - Problemas con el xml',
                        'messageDetail' => 'CA305 - La fecha de emisión no está dentro de la vigencia del CSD del Emisor.',
                        'data' => null,
                        'status' => 'error',
                    ], JSON_THROW_ON_ERROR)
                )
            );
        $mockHttpClient = new Client();
        $mockHttpClient->setDefaultResponse($clientErrorResponse);

        $sut = new Api('token', Environment::TESTING, $mockHttpClient);

        $response = $sut->cancel($this->getFixtureContents('cancellation_request.xml'));

        $this->assertFalse($response->success);
        $this->assertSame('CACFDI33 - Problemas con el xml', $response->message);
        $this->assertSame(
            'CA305 - La fecha de emisión no está dentro de la vigencia del CSD del Emisor.',
            $response->messageDetail
        );
        $this->assertSame('error', $response->status);
    }
}
