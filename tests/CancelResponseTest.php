<?php

/*
 * xint0/sw-php-client
 *
 * SmarterWeb API PHP client
 *
 * @author Rogelio Jacinto Pascual
 * @copyright Copyright 2023 Rogelio Jacinto Pascual
 * @license https://gitlab.com/xint0-open-source/sw-php-client/-/blob/main/LICENSE MIT License
 */

declare(strict_types=1);

namespace Tests;

use Http\Discovery\Psr17FactoryDiscovery;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Tests\Concerns\UsesTestFixtures;
use Xint0\SmarterWeb\CancelResponse;

#[CoversClass(CancelResponse::class)]
class CancelResponseTest extends TestCase
{
    use UsesTestFixtures;

    public function test_instance_has_expected_properties_when_response_interface_has_server_error_code_and_text_payload(): void
    {
        $response = Psr17FactoryDiscovery::findResponseFactory()->createResponse(500)
            ->withBody(Psr17FactoryDiscovery::findStreamFactory()->createStream('Error message'));
        $sut = new CancelResponse($response);

        $this->assertFalse($sut->success);
        $this->assertNull($sut->data);
        $this->assertSame('Error message', $sut->message);
        $this->assertNull($sut->messageDetail);
        $this->assertSame('error', $sut->status);
        $this->assertSame($response, $sut->getResponseInterface());
    }

    public function test_instance_has_expected_properties_when_response_interface_has_client_error_code_and_json_payload(): void
    {
        $responseData = [
            'message' => 'CACFDI33 - Problemas con el xml.',
            'messageDetail' => 'CA305 - La fecha de emisión no esta dentro de la vigencia del CSD del Emisor.',
            'data' => null,
            'status' => 'error',
        ];
        /** @noinspection PhpUnhandledExceptionInspection */
        $responseBody = json_encode($responseData, JSON_THROW_ON_ERROR);
        $response = Psr17FactoryDiscovery::findResponseFactory()->createResponse(400)
            ->withBody(Psr17FactoryDiscovery::findStreamFactory()->createStream($responseBody));

        $sut = new CancelResponse($response);
        $this->assertFalse($sut->success);
        $this->assertNull($sut->data);
        $this->assertSame($responseData['message'], $sut->message);
        $this->assertSame($responseData['messageDetail'], $sut->messageDetail);
        $this->assertSame($responseData['status'], $sut->status);
        $this->assertSame($response, $sut->getResponseInterface());
    }

    public function test_instance_has_expected_properties_when_response_interface_has_success_code_and_returns_invalid_json(): void
    {
        $responseBody = '{"data":';
        $response = Psr17FactoryDiscovery::findResponseFactory()->createResponse()
            ->withBody(Psr17FactoryDiscovery::findStreamFactory()->createStream($responseBody));

        $sut = new CancelResponse($response);
        $this->assertFalse($sut->success);
        $this->assertSame(['contents' => $responseBody], $sut->data);
        $this->assertSame('Could not decode JSON response.', $sut->message);
        $this->assertSame('Syntax error', $sut->messageDetail);
        $this->assertSame('error', $sut->status);
        $this->assertSame($response, $sut->getResponseInterface());
    }

    public function test_instance_has_expected_properties_when_response_interface_has_success_code_and_body_returns_valid_json(): void
    {
        $response = Psr17FactoryDiscovery::findResponseFactory()->createResponse()
            ->withBody(
                Psr17FactoryDiscovery::findStreamFactory()->createStreamFromFile(
                    $this->getFixtureFullPath('cancel_success_response.json')
                )
            );
        /** @var array{data:array<string,mixed>,status:string} $responseData */
        $responseData = json_decode($this->getFixtureContents('cancel_success_response.json'), true);

        $sut = new CancelResponse($response);
        $this->assertTrue($sut->success);
        $this->assertSame($responseData['data'], $sut->data);
        $this->assertEmpty($sut->message);
        $this->assertSame('success', $sut->status);
        $this->assertSame($response, $sut->getResponseInterface());
    }
}
